FROM openjdk:12
ADD target/docker-spring-boot-jackson.jar docker-spring-boot-jackson.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-jackson.jar"]
